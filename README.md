terraform-gitlab-runner
-----------------------

This project holds the configuration for our Gitlab Runners.


## How to add a new runners

This deployes runners in deployments

To deploy a new set of runners (see main.tf variables.tf) create a deployment.tfvars file at projectroot/${DEPLOYMENT}.tfvars .
Use the example baltimare.tfvars as an example. Additional options may be set see terraform/variables.tf for descriptions etc.

Pipeline control variables

DEPLOYMENT: Deployment name example: baltimare-runners
DESTROY: Destroy $DEPLOYMENT
DEREGISTER: Deregister runners for $DEPLOYMENT
DEPLOYMENT_TYPE: set in pipeline vas should be infra-deploy

```
# gitlab-runners-ovh.tf
module "<runner name>" {
  source                           = "git::https://gitlab.com/dreamer-labs/iac/terraform-gitlab-runner-module.git/modules/gitlab-runner/generic?ref=master"
  cloud_provider                   = "ovh"
  gitlab_runner_name               = "Test Runner"
  gitlab_runner_registration_token = var.<runner name>_registration_token
  gitlab_runner_api_token          = var.gitlab_runner_api_token
  gitlab_runner_description        = "My Test Runner"
  gitlab_runner_url                = "https://gitlab.com"
  deployment                       = "gitlab-runner"
  env_tag                          = "dev"
}
```

4. Ensure the `dreamerlabsbot` user has Maintainer access to the repository which you are attaching the Gitlab Runner to
