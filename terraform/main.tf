
data "openstack_images_image_v2" "centos" {
    name = "Ubuntu 20.04"
    most_recent = true
}

data "openstack_compute_flavor_v2" "flavor" {
    name = "d2-8"
}

resource "openstack_compute_keypair_v2" "gitlab_runner_keypair" {
  name = "${var.deployment}-keypair"
}

resource "null_resource" "dump-ssh-key" {

  triggers = {
    always_run = "${timestamp()}"
  }

  provisioner "local-exec" {
    command = "echo '${sensitive(openstack_compute_keypair_v2.gitlab_runner_keypair.private_key)}' >${var.deployment}-id_rsa"
  }
}

# gitlab-runner-baltimare-pinkie
resource "openstack_compute_instance_v2" "gitlab_runner" {
  count           = var.runner_count
  name            = "gitlab-runner-${var.deployment}-${var.instance_names[count.index]}"
  image_id        = data.openstack_images_image_v2.centos.id
  flavor_id       = data.openstack_compute_flavor_v2.flavor.id
  key_pair        = openstack_compute_keypair_v2.gitlab_runner_keypair.name
  security_groups = ["default"]

  metadata = {
    Environment = var.env
    Deployment  = var.deployment
    Node        = "${var.deployment}-runner"
    groups      = "${var.deployment}-runner,runner"
    Name        = "Gitlab Runner ${var.deployment} ${var.instance_names[count.index]}"
  }

  network {
    name = "Ext-Net"
  }
}

resource "openstack_blockstorage_volume_v2" "dockervol" {
  count   = var.runner_count
  name    = "${var.instance_names[count.index]}_dockervol"
  size    = var.docker_volume_size
}

resource "openstack_compute_volume_attach_v2" "attached" {
  count           = var.runner_count
  instance_id     = openstack_compute_instance_v2.gitlab_runner[count.index].id
  volume_id       = openstack_blockstorage_volume_v2.dockervol[count.index].id
}
