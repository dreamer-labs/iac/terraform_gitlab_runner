// variable "gitlab_runner_api_token" {
//   type = string
// }
//
// variable "dreamerlabs_registration_token" {
//   type = string
// }

variable "deployment" {
  type = string
}

variable "env" {
  type = string
}

variable "env_description" {
  type = string
}

variable "cloud_provider" {
  type = string
}

variable "instance_names" {
  type    = list
  default = ["pinkie", "rainbow", "twilight", "apple"]
}

variable "runner_count" {
  type    = number
  default = 1
}

variable "docker_volume_size" {
  type    = number
  default = 50
}
